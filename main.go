// main.go
package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello, Golang on Cloud Run!")
}

func main() {
	http.HandleFunc("/", handler)
	err:=http.ListenAndServe(":9090", nil)
	if err!=nil {
		fmt.Println(err)
	}
}
